---
title: "BiiLabs Meet up"
date: 2019-06-06T18:00:00+09:00
author: "solareenlo"
tags: [
  "BiiLabs",
  "Meet up"
]
categories: [
  "application"
]
menu:
  main:
    parent: tutorials
---

## BiiLabsのミートアップ
- IOTAに関する目新しいことは特になし.
- BiiLabsさんのミートアップだった.
  - IoTにDLTを応用してこんなこと出来ます.
  - 便利なAPIを公開しております.
